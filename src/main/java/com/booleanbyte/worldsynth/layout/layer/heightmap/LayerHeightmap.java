package com.booleanbyte.worldsynth.layout.layer.heightmap;

import java.io.File;
import java.util.HashMap;

import com.booleanbyte.worldsynth.composer.tool.Tool;
import com.booleanbyte.worldsynth.composer.tool.ToolCursor;
import com.booleanbyte.worldsynth.composer.tool.heightmap.ToolElevation;
import com.booleanbyte.worldsynth.composer.tool.heightmap.ToolFlatten;
import com.booleanbyte.worldsynth.composer.tool.heightmap.ToolSmoothen;
import com.booleanbyte.worldsynth.layout.Layout;
import com.booleanbyte.worldsynth.layout.layer.Layer;
import com.booleanbyte.worldsynth.layout.layer.LayerRender;
import com.sun.prism.paint.Color;

import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

public class LayerHeightmap extends Layer {
	
	final HashMap<String, HeightmapRegion> regionmap = new HashMap<String, HeightmapRegion>(256);
	private final LayerRender<LayerHeightmap> render;
	
	public LayerHeightmap(Layout parentLayout, String name) {
		super(parentLayout, name);
		render = new LayerRenderHeightmap(this);
	}
	
	@Override
	public Tool[] getLayerTools() {
		return new Tool[]{new ToolCursor(), new ToolElevation(), new ToolFlatten(), new ToolSmoothen()};
	}

	@Override
	public Image buildTumbnailImage() {
		// TODO Build tumbnail for heightmap layer
		WritableImage tumb = new WritableImage(32, 32);
		
		PixelWriter pw = tumb.getPixelWriter();
		int argb = Color.BLACK.getIntArgbPre();
		for(int u = 0; u < tumb.getWidth(); u++) {
			for(int v = 0; v < tumb.getHeight(); v++) {
				pw.setArgb(u, v, argb);
			}
		}
		
		return tumb;
	}

	@Override
	public LayerRender<LayerHeightmap> getRender() {
		return render;
	}
	
	public void applyChanges(int x, int z, float[][] change, float factor) {
		int changeWidth = change.length;
		int changeLength = change[0].length;
		
		for(int u = 0; u < changeWidth; u++) {
			for(int v = 0; v < changeLength; v++) {
				applyChangeAt(x+u, z+v, change[u][v], factor);
			}
		}
	}
	
	public void applyChangeAt(int x, int z, float change, float factor) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		HeightmapRegion region = regionmap.get(regionX + "," + regionZ);
		
		if(region == null) {
			region = new HeightmapRegion(regionX, regionZ);
			regionmap.put(regionX + "," + regionZ, region);
		}
		
		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		region.regionValues[rx][rz] = Math.min(1.0f, Math.max(0.0f, region.regionValues[rx][rz] + change * factor));
		region.queRebuildRender();
	}
	
	public void applyValues(int x, int z, float[][] value) {
		int changeWidth = value.length;
		int changeLength = value[0].length;
		
		for(int u = 0; u < changeWidth; u++) {
			for(int v = 0; v < changeLength; v++) {
				applyValueAt(x+u, z+v, value[u][v]);
			}
		}
	}
	
	public void applyValueAt(int x, int z, float value) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		HeightmapRegion region = regionmap.get(regionX + "," + regionZ);
		
		if(region == null) {
			region = new HeightmapRegion(regionX, regionZ);
			regionmap.put(regionX + "," + regionZ, region);
		}
		
		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		
		region.regionValues[rx][rz] = value;
		region.queRebuildRender();
	}
	
	public float getValueAt(int x, int z) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		HeightmapRegion region = regionmap.get(regionX + "," + regionZ);
		if(region == null) {
			return 0.0f;
		}
		
		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		return region.regionValues[rx][rz];
	}
	
	public void writeLayerToFile(File f) {
		//TODO Write heightmap layer to file
	}
	
	public void readLayerFromFile(File f) {
		//TODO Read heightmap layer from file
	}
}
