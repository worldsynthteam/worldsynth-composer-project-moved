package com.booleanbyte.worldsynth.layout.layer;

import java.io.File;

import com.booleanbyte.worldsynth.composer.tool.Tool;
import com.booleanbyte.worldsynth.composer.tool.ToolCursor;
import com.booleanbyte.worldsynth.layout.Layout;

import javafx.scene.image.Image;

public class LayerNull extends Layer {

	public LayerNull(Layout parrentLayout, String name) {
		super(parrentLayout, name);
	}

	@Override
	public Tool[] getLayerTools() {
		return new Tool[] {new ToolCursor()};
	}

	@Override
	public Image buildTumbnailImage() {
		return null;
	}

	@Override
	public LayerRender<?> getRender() {
		return null;
	}

	@Override
	public void writeLayerToFile(File f) {
		return;
	}

	@Override
	public void readLayerFromFile(File f) {
		return;
	}
}
