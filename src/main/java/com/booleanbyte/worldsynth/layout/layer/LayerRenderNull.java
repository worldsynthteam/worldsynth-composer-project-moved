package com.booleanbyte.worldsynth.layout.layer;

import com.booleanbyte.worldsynth.composer.ui.navcanvas.NavigationalCanvas;

import javafx.scene.canvas.GraphicsContext;

public class LayerRenderNull extends LayerRender<LayerNull> {

	public LayerRenderNull(LayerNull layer) {
		super(layer);
	}

	@Override
	protected void updateRender(LayerNull layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		g.clearRect(0, 0, 10000, 10000);
	}

}
