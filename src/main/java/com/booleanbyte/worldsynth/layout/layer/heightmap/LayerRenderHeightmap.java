package com.booleanbyte.worldsynth.layout.layer.heightmap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.booleanbyte.worldsynth.composer.ui.navcanvas.Coordinate;
import com.booleanbyte.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import com.booleanbyte.worldsynth.composer.ui.navcanvas.Pixel;
import com.booleanbyte.worldsynth.layout.layer.LayerRender;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class LayerRenderHeightmap extends LayerRender<LayerHeightmap> {
	
	private RenderStyle renderStyle = RenderStyle.SHADED;
	
	private final HashMap<HeightmapRegion, Image> regionRenders = new HashMap<HeightmapRegion, Image>();
	
	public LayerRenderHeightmap(LayerHeightmap layer) {
		super(layer);
	}

	@Override
	protected void updateRender(LayerHeightmap layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		g.clearRect(0, 0, 10000, 10000);
		
		Iterator<Entry<String, HeightmapRegion>> i = layer.regionmap.entrySet().iterator();
		while(i.hasNext()) {
			HeightmapRegion region = i.next().getValue();
			if(region.regionRebuildRenderIsQued()) {
				regionRenders.put(region, buildRegionRender(layer, region));
				region.edqueRebuildRender();
			}
			drawRegion(region, regionRenders.get(region), g, navCanvas);
		}
	}
	
	private void drawRegion(HeightmapRegion region, Image regionimage, GraphicsContext g, NavigationalCanvas navCanvas) {
		Pixel corner = new Pixel(new Coordinate(region.getRegionX()*256, region.getRegionZ()*256), navCanvas);
		g.drawImage(regionimage, corner.x, corner.y, 256.0*navCanvas.getZoom(), 256.0*navCanvas.getZoom());
	}
	
	private Image buildRegionRender(LayerHeightmap layer, HeightmapRegion region) {
		WritableImage regionRenderImage = new WritableImage(256, 256);
		PixelWriter pw = regionRenderImage.getPixelWriter();
		
		if(renderStyle == RenderStyle.GRAYSCALE) {
			for(int u = 0; u < 256; u++) {
				for(int v = 0; v < 256; v++) {
					pw.setColor(u, v, Color.gray(region.regionValues[u][v]));
				}
			}
		}
		else if(renderStyle == RenderStyle.SHADED) {
			for(int u = 0; u < 256; u++) {
				for(int v = 0; v < 256; v++) {
					float s = getHeightAtRegionLocal(layer, region, u, v) - getHeightAtRegionLocal(layer, region, u+1, v);
					s = Math.max(Math.min(region.regionValues[u][v] + s*10f, 1.0f), 0.0f);
					pw.setColor(u, v, Color.gray(s));
				}
			}
		}
		
		return regionRenderImage;
	}
	
	private float getHeightAtRegionLocal(LayerHeightmap layer, HeightmapRegion region, int regionLocalX, int regionLocalZ) {
		if(regionLocalX < 0 || regionLocalX >= 256) {
			return layer.getValueAt(region.getRegionX()*256+regionLocalX, region.getRegionZ()*256+regionLocalZ);
		}
		else if(regionLocalZ < 0 || regionLocalZ >= 256) {
			return layer.getValueAt(region.getRegionX()*256+regionLocalX, region.getRegionZ()*256+regionLocalZ);
		}
		return region.regionValues[regionLocalX][regionLocalZ];
	}
	
	private enum RenderStyle {
		GRAYSCALE,
		SHADED;
	}
}
