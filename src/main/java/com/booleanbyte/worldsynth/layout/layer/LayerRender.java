package com.booleanbyte.worldsynth.layout.layer;

import com.booleanbyte.worldsynth.composer.layout.LayoutViewer;
import com.booleanbyte.worldsynth.composer.ui.navcanvas.NavigationalCanvas;

import javafx.scene.canvas.GraphicsContext;

public abstract class LayerRender<T extends Layer> {
	private final T layer;
	
	public LayerRender(T layer) {
		this.layer = layer;
	}
	
	protected final T getLayer() {
		return layer;
	}
	
	public final void updateRender(GraphicsContext g, NavigationalCanvas navCanvas) {
		updateRender(layer, g, navCanvas);
	}
	
	public final void clearRender(GraphicsContext g, NavigationalCanvas navCanvas) {
		g.clearRect(0, 0, 10000, 10000);
	}
	
	protected abstract void updateRender(T layer, GraphicsContext g, NavigationalCanvas navCanvas);
}
