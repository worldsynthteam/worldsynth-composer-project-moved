package com.booleanbyte.worldsynth.layout.layer;

import java.io.File;

import com.booleanbyte.worldsynth.composer.tool.Tool;
import com.booleanbyte.worldsynth.layout.Layout;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;

public abstract class Layer {
	
	private final Layout parentLayout;
	
	private final SimpleStringProperty layerName = new SimpleStringProperty("Unnamed layer");
	private final SimpleObjectProperty<BlendMode> layerPreviewBlendMode = new SimpleObjectProperty<BlendMode>(BlendMode.ADD);
	private final SimpleObjectProperty<Image> layerTumbnail = new SimpleObjectProperty<Image>(buildTumbnailImage());
	private final SimpleBooleanProperty layerLocked = new SimpleBooleanProperty(false);
	private final SimpleBooleanProperty layerActive = new SimpleBooleanProperty(true);
	
	public Layer(Layout parentLayout, String name) {
		this.parentLayout = parentLayout;
		layerName.set(name);
	}
	
	public Layout getParentLayout() {
		return parentLayout;
	}
	
	public SimpleStringProperty layerNameProperty() {
		return layerName;
	}
	
	public void setLayerName(String layerName) {
		this.layerName.set(layerName);
	}
	
	public String getLayerName() {
		return layerName.get();
	}
	
	public SimpleObjectProperty<BlendMode> layerPreviewBlendmodeProperty() {
		return layerPreviewBlendMode;
	}
	
	public void setLayerPreviewBlemndMode(BlendMode blendMode) {
		this.layerPreviewBlendMode.set(blendMode);
	}
	
	public BlendMode getLayerPreviewBlendMode() {
		return layerPreviewBlendMode.get();
	}
	
	public SimpleObjectProperty<Image> layerTumbnailProperty() {
		return layerTumbnail;
	}
	
	public void setLayerTumbnail(Image layerTumbnail) {
		this.layerTumbnail.set(layerTumbnail);
	}
	
	public Image getLayerTumbnail() {
		return layerTumbnail.get();
	}
	
	public SimpleBooleanProperty layerActiveProperty() {
		return layerActive;
	}
	
	public void setLayerActive(boolean layerActive) {
		this.layerActive.set(layerActive);
	}
	
	public boolean getLayerActive() {
		return layerActive.get();
	}
	
	public SimpleBooleanProperty layerLockedProperty() {
		return layerLocked;
	}
	
	public void setLayerLocked(boolean layerLocked) {
		this.layerLocked.set(layerLocked);
	}
	
	public boolean getLayerLocked() {
		return layerLocked.get();
	}
	
	public abstract Tool[] getLayerTools();
	
	public abstract Image buildTumbnailImage();
	
	public abstract LayerRender<? extends Layer> getRender();
	
	public abstract void writeLayerToFile(File f);
	public abstract void readLayerFromFile(File f);
}
