package com.booleanbyte.worldsynth.layout.layer.heightmap;

public class HeightmapRegion {
	
	public final int regionX, regionZ;
	public float[][] regionValues = new float[256][256];
	
	private boolean rebuildRenderQued = true;
	
	public HeightmapRegion(int regionX, int regionZ) {
		this.regionX = regionX;
		this.regionZ = regionZ;
		
		queRebuildRender();
	}
	
	public final int getRegionX() {
		return regionX;
	}
	
	public final int getRegionZ() {
		return regionZ;
	}
	
	public final float getHeightAt(int localX, int localZ) {
		return regionValues[localX][localZ];
	}
	
	public final void queRebuildRender() {
		rebuildRenderQued = true;
	}
	
	public final void edqueRebuildRender() {
		rebuildRenderQued = false;
	}
	
	public final boolean regionRebuildRenderIsQued() {
		return rebuildRenderQued;
	}
}
