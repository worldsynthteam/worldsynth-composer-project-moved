package com.booleanbyte.worldsynth.layout;

import java.io.File;

import com.booleanbyte.worldsynth.layout.layer.Layer;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Layout {
	
	protected final SimpleStringProperty layoutName;
	protected final ObservableList<Layer> layers = FXCollections.observableArrayList();
	
	public Layout(String layoutName) {
		this.layoutName = new SimpleStringProperty(layoutName);
	}
	
	public SimpleStringProperty layoutNameProperty() {
		return layoutName;
	}
	
	public String getLayoutName() {
		return layoutName.get();
	}
	
	public void setLayoutName(String projectName) {
		this.layoutName.set(projectName);
	}
	
	public ObservableList<Layer> getObservableLayersList() {
		return layers;
	}
	
	public void addLayer(Layer layer) {
		layers.add(layer);
	}
	
	public boolean removeLayer(Layer layer) {
		return layers.remove(layer);
	}
	
	public void writeLayoutToFile(File f) {
		//TODO Write layout to file
	}
	
	public void readLayoutFromFile(File f) {
		//TODO Read layout from file
	}
}
