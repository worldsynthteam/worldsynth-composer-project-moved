package com.booleanbyte.worldsynth.composer;

import com.booleanbyte.worldsynth.composer.brush.BrushRegistry;
import com.booleanbyte.worldsynth.composer.ui.WorldSynthComposer;

import javafx.application.Application;
import javafx.stage.Stage;

public class WorldSynthComposerLauncher extends Application {
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		new BrushRegistry();
		new WorldSynthComposer(primaryStage);
	}
}
