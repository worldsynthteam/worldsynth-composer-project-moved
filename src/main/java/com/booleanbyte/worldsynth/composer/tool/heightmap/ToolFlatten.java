package com.booleanbyte.worldsynth.composer.tool.heightmap;

import com.booleanbyte.worldsynth.composer.brush.heightmap.Heightbrush;
import com.booleanbyte.worldsynth.composer.ui.WorldSynthComposer;
import com.booleanbyte.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import com.booleanbyte.worldsynth.layout.layer.heightmap.LayerHeightmap;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class ToolFlatten extends ToolHeightmap {
	
	private Heightbrush brush = null;
	
	private boolean active = false;
	
	float currentHeightSample;
	
	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getResourceAsStream("toolicon_flatten.png")));
	}

	@Override
	public void onPrimaryPressed(double x, double z, double pressure, LayerHeightmap layer) {
		currentHeightSample = layer.getValueAt((int) x, (int) z);
	}

	@Override
	public void onSecondaryPressed(double x, double z, double pressure, LayerHeightmap layer) {
	}
	
	@Override
	public void onPrimaryDown(double x, double z, double pressure, LayerHeightmap layer) {
		Heightbrush b = (Heightbrush) WorldSynthComposer.brushBrowser.getSelectedBrush();
		flatten((int) x - b.getBrushWidth()/2, (int) z - b.getBrushHeight()/2, pressure, b, layer);
	}

	@Override
	public void onSecondaryDown(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void onPrimaryDragged(double x, double z, double pressure, LayerHeightmap layer) {
		Heightbrush b = (Heightbrush) WorldSynthComposer.brushBrowser.getSelectedBrush();
		flatten((int) x - b.getBrushWidth()/2, (int) z - b.getBrushHeight()/2, pressure, b, layer);
	}

	@Override
	public void onSecondaryDragged(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void onPrimaryReleased(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void onSecondaryReleased(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void renderTool(double x, double z, GraphicsContext g, NavigationalCanvas navCanvas) {
	}
	
	private void flatten(int x, int z, double pressure, Heightbrush brush, LayerHeightmap layer) { 
		for(int u = 0; u < brush.getBrushWidth(); u++) {
			for(int v = 0; v < brush.getBrushHeight(); v++) {
				float isValue = layer.getValueAt(x+u, z+v);
				float setValue = currentHeightSample;
				float diff = setValue - isValue;
				layer.applyValueAt(x+u, z+v, isValue + diff * brush.getBrushValues()[u][v] * 0.1f);
			}
		}
	}
}
