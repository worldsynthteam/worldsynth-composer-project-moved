package com.booleanbyte.worldsynth.composer.tool;

import com.booleanbyte.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import com.booleanbyte.worldsynth.layout.layer.Layer;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class ToolCursor extends Tool<Layer> {

	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getResourceAsStream("toolicon_cursor.png")));
	}

	@Override
	protected Pane toolPane() {
		return new Pane();
	}

	@Override
	public void onPrimaryPressed(double x, double z, double pressure, Layer layer) {
	}

	@Override
	public void onSecondaryPressed(double x, double z, double pressure, Layer layer) {
	}

	@Override
	public void onPrimaryDown(double x, double z, double pressure, Layer layer) {
	}

	@Override
	public void onSecondaryDown(double x, double z, double pressure, Layer layer) {
	}

	@Override
	public void onPrimaryDragged(double x, double z, double pressure, Layer layar) {
	}

	@Override
	public void onSecondaryDragged(double x, double z, double pressure, Layer layar) {
	}

	@Override
	public void onPrimaryReleased(double x, double z, double pressure, Layer layar) {
	}

	@Override
	public void onSecondaryReleased(double x, double z, double pressure, Layer layar) {
	}

	@Override
	public void renderTool(double x, double z, GraphicsContext g, NavigationalCanvas navCanvas) {
	}
}
