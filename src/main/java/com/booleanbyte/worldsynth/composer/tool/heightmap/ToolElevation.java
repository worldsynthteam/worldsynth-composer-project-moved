package com.booleanbyte.worldsynth.composer.tool.heightmap;

import com.booleanbyte.worldsynth.composer.brush.heightmap.Heightbrush;
import com.booleanbyte.worldsynth.composer.ui.WorldSynthComposer;
import com.booleanbyte.worldsynth.composer.ui.navcanvas.Coordinate;
import com.booleanbyte.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import com.booleanbyte.worldsynth.composer.ui.navcanvas.Pixel;
import com.booleanbyte.worldsynth.layout.layer.heightmap.LayerHeightmap;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class ToolElevation extends ToolHeightmap {
	
	private Heightbrush brush = null;
	
	private boolean primaryActive = false;
	private boolean secondaryActive = false;
	
	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getResourceAsStream("toolicon_elevation.png")));
	}

	@Override
	public void onPrimaryPressed(double x, double z, double pressure, LayerHeightmap layer) {
		primaryActive = true;
		brush = (Heightbrush) WorldSynthComposer.brushBrowser.getSelectedBrush();
		if(brush == null) {
			return;
		}
		layer.applyChanges((int) x - brush.getBrushWidth()/2, (int) z - brush.getBrushHeight()/2, brush.getBrushValues(), 0.1f);
	}
	
	@Override
	public void onSecondaryPressed(double x, double z, double pressure, LayerHeightmap layer) {
		secondaryActive = true;
		brush = (Heightbrush) WorldSynthComposer.brushBrowser.getSelectedBrush();
		if(brush == null) {
			return;
		}
		layer.applyChanges((int) x - brush.getBrushWidth()/2, (int) z - brush.getBrushHeight()/2, brush.getBrushValues(), -0.1f);
	}
	
	@Override
	public void onPrimaryDown(double x, double z, double pressure, LayerHeightmap layer) {
		brush = (Heightbrush) WorldSynthComposer.brushBrowser.getSelectedBrush();
		if(brush == null) {
			return;
		}
		layer.applyChanges((int) x - brush.getBrushWidth()/2, (int) z - brush.getBrushHeight()/2, brush.getBrushValues(), 0.1f);
	}

	@Override
	public void onSecondaryDown(double x, double z, double pressure, LayerHeightmap layer) {
		brush = (Heightbrush) WorldSynthComposer.brushBrowser.getSelectedBrush();
		if(brush == null) {
			return;
		}
		layer.applyChanges((int) x - brush.getBrushWidth()/2, (int) z - brush.getBrushHeight()/2, brush.getBrushValues(), -0.1f);
	}

	@Override
	public void onPrimaryDragged(double x, double z, double pressure, LayerHeightmap layer) {
//		Heightbrush b = (Heightbrush) WorldSynthComposer.brushBrowser.getSelectedBrush();
//		if(b == null) {
//			return;
//		}
//		layer.applyChange((int) x - b.getBrushWidth()/2, (int) z - b.getBrushHeight()/2, b.getBrushValues(), 0.1f);
	}

	@Override
	public void onSecondaryDragged(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void onPrimaryReleased(double x, double z, double pressure, LayerHeightmap layer) {
		primaryActive = false;
	}

	@Override
	public void onSecondaryReleased(double x, double z, double pressure, LayerHeightmap layer) {
		secondaryActive = false;
	}

	@Override
	public void renderTool(double x, double z, GraphicsContext g, NavigationalCanvas navCanvas) {
		if(primaryActive || secondaryActive || brush == null) return;
		
		Pixel corner = new Pixel(new Coordinate(x - brush.getBrushWidth()*0.5, z - brush.getBrushHeight()*0.5), navCanvas);
		g.drawImage(brush.getBrushImage(), corner.x, corner.y, brush.getBrushWidth()*navCanvas.getZoom(), brush.getBrushHeight()*navCanvas.getZoom());
		g.setStroke(Color.WHEAT);
		g.strokeRect(corner.x, corner.y, brush.getBrushWidth()*navCanvas.getZoom(), brush.getBrushHeight()*navCanvas.getZoom());
	}
}
