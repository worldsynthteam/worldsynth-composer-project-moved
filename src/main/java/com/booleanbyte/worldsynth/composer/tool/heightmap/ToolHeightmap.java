package com.booleanbyte.worldsynth.composer.tool.heightmap;

import com.booleanbyte.worldsynth.composer.tool.Tool;
import com.booleanbyte.worldsynth.layout.layer.heightmap.LayerHeightmap;

import javafx.scene.layout.Pane;

public abstract class ToolHeightmap extends Tool<LayerHeightmap> {
	
	private static ToolHeightmap instanced;
	protected static HeightmapToolPane toolPane;
	
	public ToolHeightmap() {
		if(instanced == null) {
			toolPane = new HeightmapToolPane();
			instanced = this;
		}
	}
	
	@Override
	protected Pane toolPane() {
		return toolPane;
	}
	
	private class HeightmapToolPane extends Pane {
		
	}
}
