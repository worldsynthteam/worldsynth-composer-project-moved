package com.booleanbyte.worldsynth.composer.tool;

import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

public class ComposerToolBar extends BorderPane {
	
	ToolBar toolbar;
	Tool<?> selectedTool = new ToolCursor();
	
	public ComposerToolBar() {
		toolbar = new ToolBar();
		toolbar.setOrientation(Orientation.VERTICAL);
		setCenter(toolbar);
	}
	
	public void setTools(Tool<?>[] tools) {
		toolbar.getItems().clear();
		for(Tool<?> t: tools) {
			Button toolButton = new Button();
			toolButton.setGraphic(new ImageView(t.getToolIconImage()));
			toolButton.setOnAction(e -> {
				selectedTool = t;
			});
			toolbar.getItems().add(toolButton);
		}
	}
	
	public Tool<?> getSelectedTool() {
		return selectedTool;
	}
}
