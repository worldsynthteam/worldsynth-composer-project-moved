package com.booleanbyte.worldsynth.composer.tool;

import com.booleanbyte.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import com.booleanbyte.worldsynth.layout.layer.Layer;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public abstract class Tool<T extends Layer> {
	private Image toolIconImage;
	
	public Tool() {
		toolIconImage = toolIconImage();
	}
	
	public Image getToolIconImage() {
		return toolIconImage;
	}
	
	public final T castLayer(Layer l) {
		return (T) l;
	}
	
	protected abstract Image toolIconImage();
	
	protected abstract Pane toolPane();
	
	public abstract void onPrimaryPressed(double x, double z, double pressure, T layer);
	public abstract void onSecondaryPressed(double x, double z, double pressure, T layer);
	public abstract void onPrimaryDown(double x, double z, double pressure, T layer);
	public abstract void onSecondaryDown(double x, double z, double pressure, T layer);
	public abstract void onPrimaryDragged(double x, double z, double pressure, T layer);
	public abstract void onSecondaryDragged(double x, double z, double pressure, T layer);
	public abstract void onPrimaryReleased(double x, double z, double pressure, T layer);
	public abstract void onSecondaryReleased(double x, double z, double pressure, T layer);
	
	public abstract void renderTool(double x, double z, GraphicsContext g, NavigationalCanvas navCanvas);
}
