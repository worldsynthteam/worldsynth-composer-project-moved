package com.booleanbyte.worldsynth.composer.brush;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.booleanbyte.worldsynth.common.Commons;
import com.booleanbyte.worldsynth.composer.brush.heightmap.HeightbrushImage;

public class BrushRegistry {
	
	public static ArrayList<Brush> REGISTER = new ArrayList<Brush>();
	
	public BrushRegistry() {
		registerMaterials();
	}
	
	private void registerMaterials() {
		//Load brushes from files
		File brushesDirectory = new File(Commons.getExecutionDirectory(), "brushes");
		if(!brushesDirectory.exists()) {
			brushesDirectory.mkdir();
		}
		loadeBrushLib(brushesDirectory);
	}
	
	private void loadeBrushLib(File brushesDirectory) {
		
		if(!brushesDirectory.isDirectory()) {
			System.err.println("Lib directory \"" + brushesDirectory.getAbsolutePath() + "\" does not exist");
			return;
		}
		
		for(File sub: brushesDirectory.listFiles()) {
			if(sub.isDirectory()) {
				loadeBrushLib(sub);
			}
			else if(sub.getName().endsWith(".png")) {
				try {
					REGISTER.add(loadBrushFromFile(sub));
				} catch (IOException e) {
					System.err.println("Problem occoured while trying to read brush file: " + sub.getName());
					e.printStackTrace();
				}
			}
		}
	}
	
	private Brush loadBrushFromFile(File brushFile) throws IOException {
		System.out.println("Loading brush from file: " + brushFile.getName());
		return new HeightbrushImage(brushFile);
	}
	
	public static Brush[] getBrushes() {
		//Sort brushes alphabetically by name
		Brush[] brushSort = new Brush[REGISTER.size()];
		REGISTER.toArray(brushSort);
		
		return brushSort;
	}
}
