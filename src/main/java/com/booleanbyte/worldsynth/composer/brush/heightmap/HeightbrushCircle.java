package com.booleanbyte.worldsynth.composer.brush.heightmap;

import javafx.scene.image.Image;

public class HeightbrushCircle extends Heightbrush {
	
	private float[][] brushValues;
	
	public HeightbrushCircle(float radius) {
		super("Circle brush");
		int size = ((int) Math.ceil(radius)) * 2 + 1;
		int center = (size - 1) / 2;
		brushValues = new float[size][size];
		
		for(int u = 0; u < size; u++) {
			for(int v = 0; v < size; v++) {
				double dx = u - center;
				double dy = v - center;
				double dist = Math.sqrt(dx * dx + dy * dy);
				if(dist <= radius) {
					brushValues[u][v] = 1F;
				}
			}
		}
	}
	
	@Override
	public float[][] getBrushValues() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Image buildBrushTumbnail() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Image buildBrushImage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getBrushWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getBrushHeight() {
		// TODO Auto-generated method stub
		return 0;
	}
}
