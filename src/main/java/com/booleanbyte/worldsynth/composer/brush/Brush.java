package com.booleanbyte.worldsynth.composer.brush;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.Image;

public abstract class Brush {
	
	private final SimpleStringProperty brushName;
	private final SimpleObjectProperty<Image> brushTumbnailProperty = new SimpleObjectProperty<Image>();
	private final SimpleObjectProperty<Image> brushImageProperty = new SimpleObjectProperty<Image>();
	
	public Brush(String name) {
		brushName = new SimpleStringProperty(name);
	}
	
	public SimpleStringProperty brushNameProperty() {
		return brushName;
	}
	
	public void setBrushName(String brushName) {
		this.brushName.set(brushName);
	}
	
	public String getBrushName() {
		return brushName.get();
	}
	
	public final SimpleObjectProperty<Image> brushTumbnailProperty() {
		return brushTumbnailProperty;
	}
	
	public final void setBrushTumbnail(Image tumbnail) {
		brushTumbnailProperty.set(tumbnail);
	}
	
	public final Image getBrushTumbnail() {
		return brushTumbnailProperty.get();
	}
	
	public final SimpleObjectProperty<Image> brushImageProperty() {
		return brushImageProperty;
	}
	
	public final void setBrushImage(Image image) {
		brushImageProperty.set(image);
	}
	
	public final Image getBrushImage() {
		return brushImageProperty.get();
	}
	
	public final void updateBrushTumbnail() {
		setBrushTumbnail(buildBrushTumbnail());
	}
	
	public final void updateBrushImage() {
		setBrushImage(buildBrushImage());
	}
	
	protected abstract Image buildBrushTumbnail();
	protected abstract Image buildBrushImage();
	
	public abstract int getBrushWidth();
	public abstract int getBrushHeight();
}
