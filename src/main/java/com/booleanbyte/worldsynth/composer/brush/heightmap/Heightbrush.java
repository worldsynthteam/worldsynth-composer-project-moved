package com.booleanbyte.worldsynth.composer.brush.heightmap;

import com.booleanbyte.worldsynth.composer.brush.Brush;

public abstract class Heightbrush extends Brush {
	
	public Heightbrush(String name) {
		super(name);
	}

	public abstract float[][] getBrushValues();
	
}
