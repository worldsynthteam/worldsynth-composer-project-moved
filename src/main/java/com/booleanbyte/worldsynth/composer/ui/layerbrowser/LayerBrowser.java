package com.booleanbyte.worldsynth.composer.ui.layerbrowser;

import java.util.Iterator;

import com.booleanbyte.worldsynth.composer.layout.LayoutProjectWrapper;
import com.booleanbyte.worldsynth.composer.ui.WorldSynthComposer;
import com.booleanbyte.worldsynth.layout.layer.Layer;
import com.booleanbyte.worldsynth.layout.layer.heightmap.LayerHeightmap;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class LayerBrowser extends BorderPane {
	
	private LayoutProjectWrapper currentLayoutProject;
	
	private final VBox layersListVBox = new VBox();
	private final SimpleObjectProperty<LayerView> selectedLayerView = new SimpleObjectProperty<LayerView>();
	
	public LayerBrowser() {
		layersListVBox.setPadding(new Insets(2.0));
		layersListVBox.setSpacing(2.0);
		layersListVBox.setFocusTraversable(false);
		setFocusTraversable(false);
		
		//Setup layer preview blending pane
		Label previewBlendLabel = new Label("Preview blend:");
		previewBlendLabel.setAlignment(Pos.CENTER);
		previewBlendLabel.setMaxHeight(Double.MAX_VALUE);
		TextField previewBlendOpacityField = new TextField("100 %");
		previewBlendOpacityField.setMaxWidth(80);
		ComboBox<BlendMode> previewBlendModeSelector = new ComboBox<BlendMode>(FXCollections.observableArrayList(BlendMode.ADD, BlendMode.MULTIPLY, BlendMode.OVERLAY));
		previewBlendModeSelector.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(previewBlendModeSelector, Priority.ALWAYS);
		HBox previewBlendPane = new HBox(previewBlendLabel, previewBlendOpacityField, previewBlendModeSelector);
		previewBlendPane.setPadding(new Insets(4));
		previewBlendPane.setSpacing(4);
		setTop(previewBlendPane);
		
		//Setup layer list pane
		ScrollPane layerListScrollPane = new ScrollPane(layersListVBox);
		layerListScrollPane.setFocusTraversable(false);
		layerListScrollPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		layerListScrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		
		selectedLayerView.addListener((ObservableValue<? extends LayerView> observable, LayerView oldValue, LayerView newValue) -> {
			if(oldValue != null) {
				oldValue.setSelected(false);
				previewBlendModeSelector.valueProperty().unbindBidirectional(oldValue.getLayer().layerPreviewBlendmodeProperty());
			}
			if(newValue != null) {
				newValue.setSelected(true);
				previewBlendModeSelector.valueProperty().bindBidirectional(newValue.getLayer().layerPreviewBlendmodeProperty());
			}
		});
		
		setCenter(layerListScrollPane);
		
		//Setup "New layer" button
		MenuItem addHeightmapLayer = new MenuItem("Heightmap", new ImageView(new Image(getClass().getResourceAsStream("Height.png"))));
		addHeightmapLayer.setOnAction(e -> {
			currentLayoutProject.addLayer(new LayerHeightmap(currentLayoutProject, "New layer"));
		});
		MenuItem addColormapLayer = new MenuItem("Colormap", new ImageView(new Image(getClass().getResourceAsStream("Color.png"))));
		MenuItem addBiomemapLayer = new MenuItem("Biomemap", new ImageView(new Image(getClass().getResourceAsStream("Biome.png"))));
		MenuItem addMaterialmapLayer = new MenuItem("Materialmap", new ImageView(new Image(getClass().getResourceAsStream("Material.png"))));
		MenuItem addFeaturemapLayer = new MenuItem("Featuremap", new ImageView(new Image(getClass().getResourceAsStream("Feature.png"))));
		MenuItem addCurvemapLayer = new MenuItem("Curvemap", new ImageView(new Image(getClass().getResourceAsStream("Curve.png"))));
		MenuButton addLayerButton = new MenuButton("", new ImageView(new Image(getClass().getResourceAsStream("New.png"))),
				addHeightmapLayer, addColormapLayer, addBiomemapLayer, addMaterialmapLayer, addFeaturemapLayer, addCurvemapLayer);
		Tooltip.install(addLayerButton, new Tooltip("New layer"));
		
		//Setup "Remove layer" button
		Button removeLayerButton = new Button("", new ImageView(new Image(getClass().getResourceAsStream("Remove.png"))));
		Tooltip.install(removeLayerButton, new Tooltip("Remove layer"));
		removeLayerButton.setOnAction(e -> {
			currentLayoutProject.removeLayer(currentLayoutProject.getSelectedLayer());
		});
		
		setBottom(new HBox(addLayerButton, removeLayerButton));
	}
	
	
	private ChangeListener<Object> layerValueChangeListener = (ObservableValue<? extends Object> observable, Object oldValue, Object newValue) -> {
		WorldSynthComposer.updatePreview();
	};
	
	private void addLayerValueListener(Layer l) {
		l.layerActiveProperty().addListener(layerValueChangeListener);
		l.layerPreviewBlendmodeProperty().addListener(layerValueChangeListener);
	}
	
	private void removeLayerValueListener(Layer l) {
		l.layerActiveProperty().removeListener(layerValueChangeListener);
		l.layerPreviewBlendmodeProperty().removeListener(layerValueChangeListener);
	}
	
	private ListChangeListener<Layer> layerListListener = c -> {
		while(c.next()) {
			for(Layer l: c.getRemoved()) {
				browserRemoveLayer(l);
				removeLayerValueListener(l);
			}
			for(Layer l: c.getAddedSubList()) {
				browserAddLayer(l);
				addLayerValueListener(l);
			}
		}
	};
	
	public void setLayoutProject(LayoutProjectWrapper layoutProjectWrapper) {
		if(currentLayoutProject != null) {
			selectedLayerView.set(null);
			currentLayoutProject.getObservableLayersList().removeListener(layerListListener);
			for(Layer l: currentLayoutProject.getObservableLayersList()) {
				removeLayerValueListener(l);
			}
		}
		currentLayoutProject = layoutProjectWrapper;
		currentLayoutProject.getObservableLayersList().addListener(layerListListener);
		for(Layer l: currentLayoutProject.getObservableLayersList()) {
			addLayerValueListener(l);
		}
		
		layersListVBox.getChildren().clear();
		Iterator<Layer> i = currentLayoutProject.getObservableLayersList().iterator();
		while(i.hasNext()) {
			LayerView view = browserAddLayer(i.next());
			if(view.getLayer() == currentLayoutProject.getSelectedLayer()) {
				selectedLayerView.set(view);
			}
		}
	}
	
	void setSelectedLayerView(LayerView layerView) {
		selectedLayerView.set(layerView);
		currentLayoutProject.setSelectedLayer(layerView.getLayer());
	}
	
	private LayerView browserAddLayer(Layer layer) {
		LayerView view = new LayerView(layer, this);
		layersListVBox.getChildren().add(view);
		return view;
	}
	
	private void browserRemoveLayer(Layer layer) {
		for(Node lpe: layersListVBox.getChildren()) {
			LayerView entry = (LayerView) lpe;
			if(entry.getLayer() == layer) {
				layersListVBox.getChildren().remove(entry);
				return;
			}
		}
	}
}
