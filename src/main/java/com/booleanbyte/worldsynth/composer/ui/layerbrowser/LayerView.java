package com.booleanbyte.worldsynth.composer.ui.layerbrowser;

import com.booleanbyte.worldsynth.composer.ui.WorldSynthComposer;
import com.booleanbyte.worldsynth.layout.layer.Layer;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.css.PseudoClass;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

public class LayerView extends HBox {
	
	private final Layer layer;
	
	private final LayerBrowser parentLayerBrowser;
	
	private final TextField layerNameField;
	private final Label layerNameLabel;
	private final CheckBox layerLockedCheckbox;
	private final CheckBox layerActiveCheckbox;
	
	public LayerView(Layer layer, LayerBrowser parentLayerBrowser) {
		this.layer = layer;
		this.parentLayerBrowser = parentLayerBrowser;
		
		setSpacing(5.0);
		setAlignment(Pos.CENTER);
		setPrefWidth(260);
		getStyleClass().add("layer");
		
		ImageView layerTumbnailView = new ImageView();
		Bindings.bindBidirectional(layerTumbnailView.imageProperty(), layer.layerTumbnailProperty());
		
		layerNameField = new TextField();
		HBox.setHgrow(layerNameField, Priority.ALWAYS);
		Bindings.bindBidirectional(layerNameField.textProperty(), layer.layerNameProperty());
		layerNameLabel = new Label();
		Bindings.bindBidirectional(layerNameLabel.textProperty(), layer.layerNameProperty());
		Pane fillPane = new Pane();
		HBox.setHgrow(fillPane, Priority.ALWAYS);
		
		layerLockedCheckbox = new CheckBox();
		layerLockedCheckbox.setSelected(layer.getLayerLocked());
		Tooltip.install(layerLockedCheckbox, new Tooltip("Lock layer"));
		layerLockedCheckbox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			layer.setLayerLocked(newValue);
		});
		
		layerActiveCheckbox = new CheckBox();
		layerActiveCheckbox.setSelected(layer.getLayerActive());
		Tooltip.install(layerActiveCheckbox, new Tooltip("Show layer"));
		layerActiveCheckbox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			layer.setLayerActive(newValue);
		});
		
		getChildren().setAll(layerTumbnailView, layerNameLabel, fillPane, layerLockedCheckbox, layerActiveCheckbox);
		
		setOnMouseClicked(e -> {
			parentLayerBrowser.setSelectedLayerView(this);
			requestFocus();
		});
	}
	
	void setSelected(boolean selected) {
		if(selected) {
			getChildren().set(1, layerNameField);
			pseudoClassStateChanged(PseudoClass.getPseudoClass("selected"), true);
		}
		else {
			getChildren().set(1, layerNameLabel);
			pseudoClassStateChanged(PseudoClass.getPseudoClass("selected"), false);
		}
	}
	
	public Layer getLayer() {
		return layer;
	}
}
