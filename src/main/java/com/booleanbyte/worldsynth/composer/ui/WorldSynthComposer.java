package com.booleanbyte.worldsynth.composer.ui;

import java.util.HashMap;

import com.booleanbyte.worldsynth.composer.layout.LayoutProjectWrapper;
import com.booleanbyte.worldsynth.composer.layout.LayoutViewer;
import com.booleanbyte.worldsynth.composer.tool.ComposerToolBar;
import com.booleanbyte.worldsynth.composer.tool.Tool;
import com.booleanbyte.worldsynth.composer.tool.ToolCursor;
import com.booleanbyte.worldsynth.composer.ui.brushbrowser.BrushBrowser;
import com.booleanbyte.worldsynth.composer.ui.layerbrowser.LayerBrowser;

import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class WorldSynthComposer {
	//The main stage for WorldDesigner
	private final Stage stage;
	
	//Content
	public static MenuBar menuBar;
	public static final ComposerToolBar toolbar = new ComposerToolBar();;
	public static final TabPane projectTabPane = new TabPane();
	public static final LayoutViewer layoutViewer = new LayoutViewer();
	public static final BrushBrowser brushBrowser = new BrushBrowser();
	public static final LayerBrowser layersList = new LayerBrowser();
	
	//Projects
	private final HashMap<Tab, LayoutProjectWrapper> projects = new HashMap<Tab, LayoutProjectWrapper>();
	
	public WorldSynthComposer(Stage stage) throws Exception {
		this.stage = stage;
		stage.initStyle(StageStyle.DECORATED);
		stage.setTitle("WorldSynth Composer pre-alpha");
		stage.setMinWidth(800);
		stage.setMinHeight(700);
		Image stageIcon = new Image(getClass().getResourceAsStream("worldSynthIcon.png"));
		stage.getIcons().add(stageIcon);
		
		//Build up main UI
		//Add menu bar at top
		BorderPane root = new BorderPane();
		menuBar = new MenuBar(new Menu("File"), new Menu("Edit"), new Menu("Help"));
		root.setTop(menuBar);
		
		//Add toolbar at left side
		toolbar.setTools(new Tool[] {new ToolCursor()});
		root.setLeft(toolbar);
		
		//Add project tabs
		projectTabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
			setAsCurrentProject(projects.get(newValue));
		});
		root.setCenter(projectTabPane);
		
		//Add right side content
		root.setRight(new VBox(brushBrowser, layersList));
		
		//Setup the scene and style
		Scene scene = new Scene(root, 1500, 800);
		scene.getStylesheets().add(getClass().getResource("WorldDesignerMainStyle.css").toExternalForm());
		
		stage.setScene(scene);
		stage.show();
		

		addNewlayoutProject("Untitled project 1");
//		addNewlayoutProject("Untitled project 2");
//		addNewlayoutProject("Untitled project 3");
	}
	
	private void addNewlayoutProject(String layoutProjectName) {
		Tab projectTab = new Tab(layoutProjectName);
		LayoutProjectWrapper newLayoutProject = new LayoutProjectWrapper(layoutProjectName);
		projectTab.setContent(layoutViewer);
		projects.put(projectTab, newLayoutProject);
		projectTabPane.getTabs().add(projectTab);
	}
	
	private void setAsCurrentProject(LayoutProjectWrapper layoutProject) {
		layoutViewer.setLayoutProject(layoutProject);
		toolbar.setTools(layoutProject.getSelectedLayer().getLayerTools());
		layersList.setLayoutProject(layoutProject);
	}
	
	public static void updatePreview() {
		layoutViewer.updatePreview();
	}
}
