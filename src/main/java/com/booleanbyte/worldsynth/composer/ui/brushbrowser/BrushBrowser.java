package com.booleanbyte.worldsynth.composer.ui.brushbrowser;

import com.booleanbyte.worldsynth.composer.brush.Brush;
import com.booleanbyte.worldsynth.composer.brush.BrushRegistry;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;

public class BrushBrowser extends BorderPane {
	
	private final SimpleObjectProperty<BrushView> selectedBrushView = new SimpleObjectProperty<BrushView>();
	private final SimpleObjectProperty<Brush> selectedBrush = new SimpleObjectProperty<Brush>();
	FlowPane brushCatalogueFlowPane = new FlowPane();
	
	public BrushBrowser() {
		brushCatalogueFlowPane.setPadding(new Insets(8.0));
		brushCatalogueFlowPane.setVgap(8.0);
		brushCatalogueFlowPane.setHgap(8.0);
		brushCatalogueFlowPane.setPrefWidth(260);
		setMinHeight(200.0);
		setMaxHeight(300.0);
		
		ScrollPane layerListScrollPane = new ScrollPane(brushCatalogueFlowPane);
		layerListScrollPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		layerListScrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		setCenter(layerListScrollPane);
		
		selectedBrushView.addListener((ObservableValue<? extends BrushView> observable, BrushView oldValue, BrushView newValue) -> {
			if(oldValue != null) {
				oldValue.setSelected(false);
			}
			if(newValue != null) {
				newValue.setSelected(true);
			}
		});
		
		for(Brush b: BrushRegistry.getBrushes()) {
			addBrush(b);
		}
	}
	
	public void addBrush(Brush brush) {
		brushCatalogueFlowPane.getChildren().add(new BrushView(brush, this));
	}
	
	void setBrushSelection(BrushView newBrushView) {
		selectedBrushView.set(newBrushView);
		selectedBrush.set(newBrushView.getBrush());
	}
	
	public SimpleObjectProperty<Brush> selectedBrushProperty() {
		return selectedBrush;
	}
	
	public Brush getSelectedBrush() {
		return selectedBrush.get();
	}
}
