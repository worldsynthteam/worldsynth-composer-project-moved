package com.booleanbyte.worldsynth.composer.layout;

import com.booleanbyte.worldsynth.composer.ui.WorldSynthComposer;
import com.booleanbyte.worldsynth.layout.Layout;
import com.booleanbyte.worldsynth.layout.layer.Layer;
import com.booleanbyte.worldsynth.layout.layer.LayerNull;
import com.booleanbyte.worldsynth.layout.layer.heightmap.LayerHeightmap;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;

public class LayoutProjectWrapper extends Layout {
	
	private final SimpleObjectProperty<Layer> selectedLayer = new SimpleObjectProperty<Layer>(new LayerNull(this, "null"));
	
	public LayoutProjectWrapper(String layoutName) {
		super(layoutName);

		layers.add(new LayerHeightmap(this, "Default layer"));
		setSelectedLayer(layers.get(0));
		
		selectedLayerProperty().addListener((ObservableValue<? extends Layer> observable, Layer oldValue, Layer newValue) -> {
			WorldSynthComposer.toolbar.setTools(newValue.getLayerTools());
		});
	}
	
	public SimpleObjectProperty<Layer> selectedLayerProperty() {
		return selectedLayer;
	}
	
	public Layer getSelectedLayer() {
		return selectedLayer.get();
	}
	
	public void setSelectedLayer(Layer selectedLayer) {
		this.selectedLayer.set(selectedLayer);
	}
}
